﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnitTestingExample.Model;

namespace UnitTestingExample.Contracts
{
    public interface IShoppingCartService
    {
        IEnumerable<ShoppingItem> GetAllItems();
        void Add(ShoppingItem newItem);
        ShoppingItem GetById(Guid id);
    }
}
