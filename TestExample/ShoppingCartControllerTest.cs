using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using UnitTestingExample.Controllers;
using UnitTestingExample.Model;
using UnitTestingExample.Services;
using Xunit;

namespace TestExample
{
    public class ShoppingCartControllerTest
    {
        ShoppingCartController _controller;

        public ShoppingCartControllerTest()
        {
            _controller = new ShoppingCartController(new ShoppingCartService());
        }

        [Fact]
        public void Get_WhenCalled_ReturnsOkResult()
        {
            var okResult = _controller.Get();

            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void Get_WhenCalled_ReturnsAllItems()
        {
            // Arrange

            // Act
            var monkey = _controller.Get().Result as OkObjectResult;

            // Assert
            var items = Assert.IsType<List<ShoppingItem>>(monkey.Value);

            Assert.Equal(3, items.Count);
        }

        [Fact]
        public void GetById_UnknownGuidPassed_ReturnsNotFoundResult()
        {
            var notFoundResult = _controller.Get(Guid.NewGuid());

            Assert.IsType<NotFoundObjectResult>(notFoundResult.Result);
        }

        [Fact]
        public void GetById_GoodGuidPassed_ReturnsOkResult()
        {
            var okResult = _controller.Get(new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200"));

            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetById_GoodGuidPassed_RightItem()
        {
            var okResult = _controller.Get(new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200")).Result as OkObjectResult;

            Assert.IsType<ShoppingItem>(okResult.Value);
            Assert.Equal(new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200"), (okResult.Value as ShoppingItem).Id);
        }

    }
}
